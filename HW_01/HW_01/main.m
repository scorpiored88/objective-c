//
//  main.m
//  HW_01
//
//  Created by Vitaliy Heras on 11/2/16.
//  Copyright © 2016 Vitaliy Heras. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
