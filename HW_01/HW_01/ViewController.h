//
//  ViewController.h
//  HW_01
//
//  Created by Vitaliy Heras on 11/2/16.
//  Copyright © 2016 Vitaliy Heras. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AudioToolbox/AudioToolbox.h>


@interface ViewController : UIViewController <UITableViewDataSource, UITableViewDelegate>


@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UILabel *ageLabel;
@property (weak, nonatomic) IBOutlet UILabel *colorLabel;
@property (weak, nonatomic) IBOutlet UIImageView *aminalImg;
@property (strong,nonatomic) NSMutableArray* animals;

//@property (strong,nonatomic) NSMutableArray* firstAnimalsNameArray;

@property (weak, nonatomic) IBOutlet UITextField *addName;
@property (weak, nonatomic) IBOutlet UITextField *addAge;
@property (weak, nonatomic) IBOutlet UITextField *addColor;

@property (weak, nonatomic) IBOutlet UIView *addAnimalWindow;

@property (weak, nonatomic) IBOutlet UITableView *tableView;




- (void) initAnimals;
- (IBAction)sayYourName:(UIButton *)sender;


@end

