//
//  AddAnimal.h
//  HW_01
//
//  Created by Vitaliy Heras on 11/6/16.
//  Copyright © 2016 Vitaliy Heras. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Animals.h"
@interface AddAnimal : UIViewController
-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event;
- (IBAction)cancelBtn:(UIButton *)sender;

@end
