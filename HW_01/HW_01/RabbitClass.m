//
//  RabbitClass.m
//  HW_01
//
//  Created by Vitaliy Heras on 11/10/16.
//  Copyright © 2016 Vitaliy Heras. All rights reserved.
//

#import "RabbitClass.h"

@implementation RabbitClass
-(NSString*) voice {
    return @"I can`t speak :( ";
}
@end
