


#import "ViewController.h"
#import "CatClass.h"
#import "LionClass.h"
#import "CowClass.h"
#import "DogClass.h"
#import "RabbitClass.h"




#import "ViewController.h"



@interface ViewController () {
    NSInteger _counter;
    NSArray* firstAnimalsNameArray;
    
}


@end


@implementation ViewController

@synthesize nameLabel, ageLabel, colorLabel, addAnimalWindow,addName,addAge,addColor;

-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [self.view endEditing:YES];
}





-(void)showAdd :(BOOL)isVisible {
    [UIView transitionWithView:addAnimalWindow
                      duration:0.4
                       options:UIViewAnimationOptionTransitionCrossDissolve
                    animations:^{
                        addAnimalWindow.hidden = !isVisible;
                    }
                    completion:NULL];
}

- (IBAction)showAddView:(UIBarButtonItem *)sender {
    [self showAdd:YES];
}


- (IBAction)confirmAddButton:(UIButton *)sender {
    Animals* currentAnimal = [[Animals alloc] init];
    currentAnimal.name = addName.text;
    currentAnimal.age = [NSNumber numberWithInt:[addAge.text intValue]];
    currentAnimal.ownColor = addColor.text;
    addName.text = @"";
    addAge.text = @"";
    addColor.text = @"";
    
    NSLog(@"say -- %@",[currentAnimal voice]);
    
    
    
    [_animals addObject:currentAnimal];
    
    

    
    [self.tableView beginUpdates];
//
    NSIndexSet* insertSection = [NSIndexSet indexSetWithIndex:0];
//
    [self.tableView insertSections:insertSection withRowAnimation:UITableViewRowAnimationTop];
    
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:0];
    NSArray *indexPaths = [[NSArray alloc] initWithObjects:indexPath, nil];
    [self.tableView reloadRowsAtIndexPaths:indexPaths withRowAnimation:UITableViewRowAnimationNone];
    
//
    [self.tableView endUpdates];
    
    [self.view endEditing:YES];
    AudioServicesPlaySystemSound(1031);
    


//    [self.tableView reloadData];
    
    [self showAdd:NO];
        
    
    
    
    
    
}


- (IBAction)cancelAddButton:(UIButton *)sender {
    [self showAdd:NO];
}



- (void) showAnimal :(BOOL) isNext {
    if (isNext) {
        _counter++;
        if (_counter == [_animals count]) {
            _counter = 0;
        }
        nameLabel.text = [[_animals objectAtIndex:_counter] name];
        ageLabel.text = [NSString stringWithFormat:@"%@",[[_animals objectAtIndex:_counter] age]];
        colorLabel.text = [[_animals objectAtIndex:_counter] ownColor];
        NSString* imgName = [firstAnimalsNameArray containsObject:[[_animals objectAtIndex:_counter] name]]? [NSString stringWithFormat:@"%@.png",[[_animals objectAtIndex:_counter] name]]:[NSString stringWithFormat:@"Tiere-coloured"];
        _aminalImg.image = [UIImage imageNamed:imgName];
        
    } else {
        if (_counter == 0) {
            _counter = [_animals count];
        }
            nameLabel.text = [[_animals objectAtIndex:(_counter - 1)] name];
            ageLabel.text = [NSString stringWithFormat:@"%@",[[_animals objectAtIndex:(_counter -1)] age]];
            colorLabel.text = [[_animals objectAtIndex:(_counter -1)] ownColor];
            NSString* imgName = [firstAnimalsNameArray containsObject:[[_animals objectAtIndex:(_counter -1)] name]]? [NSString stringWithFormat:@"%@.png",[[_animals objectAtIndex:(_counter - 1)] name]]:[NSString stringWithFormat:@"Tiere-coloured"];
            
            _aminalImg.image = [UIImage imageNamed:imgName];
            --_counter;
        
    }


}


- (IBAction)nextBtn:(UIButton *)sender {
    AudioServicesPlaySystemSound(1104);
    [self showAnimal:YES];

}


- (IBAction)backBtn:(UIButton *)sender {
    AudioServicesPlaySystemSound(1103);
    [self showAnimal:NO];
}


- (void) initAnimals {
    CatClass * cat = [[CatClass alloc] init];
    LionClass * lion = [[LionClass alloc] init];
    DogClass * dog = [[DogClass alloc] init];
    CowClass * cow = [[CowClass alloc] init];
    RabbitClass * rabbit = [[RabbitClass alloc] init];
    
    
    cat.name = @"kitten";
    cat.age = @1;
    cat.ownColor = @"gary";
    
    
    lion.name = @"Lio";
    lion.age = @5;
    lion.ownColor = @"yelow";
    
    
    
    dog.name = @"Rex";
    dog.age = @15;
    dog.ownColor = @"black";
    
    
    
    cow.name = @"Milka";
    cow.age = @11;
    cow.ownColor = @"black&white";
    
    
    
    rabbit.name = @"banny";
    rabbit.age = @1;
    rabbit.ownColor = @"white";
    
    
    
    _animals = [NSMutableArray arrayWithObjects:cat,lion,dog,cow,rabbit,nil];
    firstAnimalsNameArray = [NSArray arrayWithObjects:cat.name,lion.name,dog.name,cow.name,rabbit.name,nil];
};


- (void)viewDidLoad {
    
    
    
    [self initAnimals];
    addAnimalWindow.hidden = YES;
    
    nameLabel.text = [[_animals objectAtIndex:0] name];
    ageLabel.text = [NSString stringWithFormat:@"%@",[[_animals objectAtIndex:0] age]];
    colorLabel.text = [[_animals objectAtIndex:0] ownColor];
    NSString* imgName = [NSString stringWithFormat:@"%@.png",[[_animals objectAtIndex:0] name]];
    _aminalImg.image = [UIImage imageNamed:imgName];
    
    _counter = 0;
    
    // Do any additional setup after loading the view, typically from a nib.
    [super viewDidLoad];
    
    self.tableView.dataSource = self;
    self.tableView.delegate = self;
//    self.animalsTable.dataSource = self;
//    self.animalsTable.delegate = self;
    
    
}



- (IBAction)sayYourName:(UIButton *)sender {
    
    [self voice:_counter];
    
}



-(void) voice :(NSInteger) currentAnimal {


    
    
    NSString *name = [NSString stringWithFormat:@"%@",[[_animals objectAtIndex:currentAnimal]name]];
    NSString* voiceDescription = [NSString stringWithFormat:@"%@", [[_animals objectAtIndex:currentAnimal]voice]];
    NSString* introduce = [NSString stringWithFormat:@"My name is %@",name];
    
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:introduce message:voiceDescription preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
    [alertController addAction:ok];
    [self presentViewController:alertController animated:YES completion:nil];
    
    if ([firstAnimalsNameArray containsObject:name] && ![name isEqual:@"banny"]) {
        SystemSoundID SoundID;
        NSString* pathToVoice = [[NSBundle mainBundle] pathForResource:name ofType:@"wav"];
        AudioServicesCreateSystemSoundID((__bridge CFURLRef) [NSURL fileURLWithPath:pathToVoice], &SoundID);
        AudioServicesPlaySystemSound(SoundID);
    }
    
    
    
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}




- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [_animals count];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString * cellId = @"Cell";
    UITableViewCell* cell = [tableView dequeueReusableCellWithIdentifier:cellId];
    if (cell == Nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:cellId];
    }
    
    UIImage* simpleAnimals = [UIImage imageNamed:@"Tiere-coloured.png"];
    cell.imageView.image = simpleAnimals;
    
//    UIImage* animalImg =[UIImage imageNamed: [NSString stringWithFormat:@"%@.png",[[_animals objectAtIndex: indexPath.row] name]]];
//    
//    cell.imageView.image = animalImg;
    
    cell.textLabel.text = [[_animals objectAtIndex: indexPath.row] name];
    
    NSString* deatail = [NSString stringWithFormat:@"age: %@; color: %@" ,[[_animals objectAtIndex: indexPath.row] age],[[_animals objectAtIndex: indexPath.row] ownColor]];
    cell.detailTextLabel.text =  deatail;
    
    return cell;
}


-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [self voice:indexPath.row];
    
}



- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    ViewController *lController = segue.destinationViewController;

    lController.animals = self.animals;
}


@end
