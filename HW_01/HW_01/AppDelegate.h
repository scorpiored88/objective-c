//
//  AppDelegate.h
//  HW_01
//
//  Created by Vitaliy Heras on 11/2/16.
//  Copyright © 2016 Vitaliy Heras. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

