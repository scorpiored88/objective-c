//
//  Animals.h
//  HW_01
//
//  Created by Vitaliy Heras on 11/2/16.
//  Copyright © 2016 Vitaliy Heras. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Animals : NSObject
@property (strong,nonatomic)NSString* name;
@property (assign,nonatomic)NSNumber* age;
@property (strong,nonatomic)NSString* ownColor;
-(NSString*) voice;
@end
