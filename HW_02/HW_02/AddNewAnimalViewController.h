//
//  AddNewAnimalViewController.h
//  HW_02
//
//  Created by Vitaliy Heras on 12/6/16.
//  Copyright © 2016 Vitaliy Heras. All rights reserved.
//

#import "ViewController.h"


@interface AddNewAnimalViewController : ViewController <UIPickerViewDataSource,UIPickerViewDelegate>

@property (weak, nonatomic) IBOutlet UITextField *customAnimalName;
@property (weak, nonatomic) IBOutlet UITextField *customAnimalColor;
@property (weak, nonatomic) IBOutlet UITextField *customAnimalAge;
@property (weak, nonatomic) IBOutlet UIPickerView *customAnimalType;



@property (weak, nonatomic) IBOutlet UIButton *SaveBtn;


@end
