//
//  AddNewAnimalViewController.m
//  HW_02
//
//  Created by Vitaliy Heras on 12/6/16.
//  Copyright © 2016 Vitaliy Heras. All rights reserved.
//

#import "AddNewAnimalViewController.h"
#import "DataBaseClass.h"
#import "addDataClass.h"

@interface AddNewAnimalViewController () {
    NSArray* animalsTypes;
    NSString* currentType;
}


@end

@implementation AddNewAnimalViewController


-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [self.view endEditing:YES];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    _customAnimalType.delegate = self;
    _customAnimalType.dataSource = self;
    
    animalsTypes = [NSArray arrayWithObjects:@"Cat",@"Cow",@"Lions",nil];
    
//    _customAnimalType.accessibilityValue
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - Picker

-(NSInteger) numberOfComponentsInPickerView:(UIPickerView *)pickerView {

    return 1;
}

-(NSInteger) pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
    return [animalsTypes count];
}

-(CGFloat) pickerView:(UIPickerView *)pickerView rowHeightForComponent:(NSInteger)component {
    return 30.0;
}

-(NSString*) pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {
    return [animalsTypes objectAtIndex:row];
}

- (void) pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component {
    currentType = [NSString stringWithFormat:@"%@",[animalsTypes objectAtIndex:row]];
    NSLog(@"---- PICKER selected %@",currentType);
}


#pragma mark - SaveButtonAction & CancelButton 

- (IBAction)SaveCustomAnimal:(UIButton *)sender {
    addDataClass* customAnimal = [[addDataClass alloc]init];
    DataBaseClass* dataToDataBase = [[DataBaseClass alloc]init];
    
    customAnimal.name = [NSString stringWithFormat:@"%@",self.customAnimalName.text];
    customAnimal.color = [NSString stringWithFormat:@"%@",self.customAnimalColor.text];
    customAnimal.animalType = [NSString stringWithFormat:@"%@",currentType];
    customAnimal.age = [NSNumber numberWithInt:[self.customAnimalAge.text intValue]];
    
    customAnimal.delgate = dataToDataBase;
    
    [customAnimal pushData];
    
    
    
    [self dismissViewControllerAnimated:YES completion:NULL];
//    [self dismissModalViewControllerAnimated:YES];

}

- (IBAction)CancelBtn:(UIBarButtonItem *)sender {
    [self dismissViewControllerAnimated:YES completion:NULL];
}





/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
