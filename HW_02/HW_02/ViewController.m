//
//  ViewController.m
//  HW_02
//
//  Created by Vitaliy Heras on 12/6/16.
//  Copyright © 2016 Vitaliy Heras. All rights reserved.
//

#import "ViewController.h"
//#import "AnimalClass.h"

@interface ViewController () {
    NSInteger _counter;
    NSArray* firstAnimalsNameArray;
    
}


@end


@implementation ViewController

@synthesize nameLabel, ageLabel, colorLabel;

-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [self.view endEditing:YES];
}














- (void) showAnimal :(BOOL) isNext {
    if (isNext) {
        _counter++;
        if (_counter == [_animals count]) {
            _counter = 0;
        }
        nameLabel.text = [[_animals objectAtIndex:_counter] name];
        ageLabel.text = [NSString stringWithFormat:@"%@",[[_animals objectAtIndex:_counter] age]];
        colorLabel.text = [[_animals objectAtIndex:_counter] ownColor];
        NSString* imgName = [firstAnimalsNameArray containsObject:[[_animals objectAtIndex:_counter] name]]? [NSString stringWithFormat:@"%@.png",[[_animals objectAtIndex:_counter] name]]:[NSString stringWithFormat:@"Tiere-coloured"];
        _aminalImg.image = [UIImage imageNamed:imgName];
        
    } else {
        if (_counter == 0) {
            _counter = [_animals count];
        }
        nameLabel.text = [[_animals objectAtIndex:(_counter - 1)] name];
        ageLabel.text = [NSString stringWithFormat:@"%@",[[_animals objectAtIndex:(_counter -1)] age]];
        colorLabel.text = [[_animals objectAtIndex:(_counter -1)] ownColor];
        NSString* imgName = [firstAnimalsNameArray containsObject:[[_animals objectAtIndex:(_counter -1)] name]]? [NSString stringWithFormat:@"%@.png",[[_animals objectAtIndex:(_counter - 1)] name]]:[NSString stringWithFormat:@"Tiere-coloured"];
        
        _aminalImg.image = [UIImage imageNamed:imgName];
        --_counter;
        
    }
    
    
}


- (IBAction)nextBtn:(UIButton *)sender {
    AudioServicesPlaySystemSound(1104);
    [self showAnimal:YES];
    
}


- (IBAction)backBtn:(UIButton *)sender {
    AudioServicesPlaySystemSound(1103);
    [self showAnimal:NO];
}


- (void) initAnimals {
    
    AnimalClass* cat = [[AnimalClass alloc] init];
    AnimalClass * lion = [[AnimalClass alloc] init];
    AnimalClass * dog = [[AnimalClass alloc] init];
    AnimalClass * cow = [[AnimalClass alloc] init];
    AnimalClass * rabbit = [[AnimalClass alloc] init];

    
    cat.name = @"kitten";
    cat.age = @1;
    cat.ownColor = @"gary";
    
    
    lion.name = @"Lio";
    lion.age = @5;
    lion.ownColor = @"yelow";
    
    
    
    dog.name = @"Rex";
    dog.age = @15;
    dog.ownColor = @"black";
    
    
    
    cow.name = @"Milka";
    cow.age = @11;
    cow.ownColor = @"black&white";
    
    
    
    rabbit.name = @"banny";
    rabbit.age = @1;
    rabbit.ownColor = @"white";
    
    
    
    _animals = [NSMutableArray arrayWithObjects:cat,lion,dog,cow,rabbit,nil];
    firstAnimalsNameArray = [NSArray arrayWithObjects:cat.name,lion.name,dog.name,cow.name,rabbit.name,nil];
};


- (void)viewDidLoad {
    
    
    
    [self initAnimals];
//    addAnimalWindow.hidden = YES;
    
    nameLabel.text = [[_animals objectAtIndex:0] name];
    ageLabel.text = [NSString stringWithFormat:@"%@",[[_animals objectAtIndex:0] age]];
    colorLabel.text = [[_animals objectAtIndex:0] ownColor];
    NSString* imgName = [NSString stringWithFormat:@"%@.png",[[_animals objectAtIndex:0] name]];
    _aminalImg.image = [UIImage imageNamed:imgName];
    
    _counter = 0;
    
    // Do any additional setup after loading the view, typically from a nib.
    [super viewDidLoad];
    
//    self.tableView.dataSource = self;
//    self.tableView.delegate = self;
    //    self.animalsTable.dataSource = self;
    //    self.animalsTable.delegate = self;
    
    
}



- (IBAction)sayYourName:(UIButton *)sender {
    
    [self voice:_counter];
    
}



-(void) voice :(NSInteger) currentAnimal {
    
    
    
    
    NSString *name = [NSString stringWithFormat:@"%@",[[_animals objectAtIndex:currentAnimal]name]];
    NSString* voiceDescription = [NSString stringWithFormat:@"%@", [[_animals objectAtIndex:currentAnimal]voice]];
    NSString* introduce = [NSString stringWithFormat:@"My name is %@",name];
    
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:introduce message:voiceDescription preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
    [alertController addAction:ok];
    [self presentViewController:alertController animated:YES completion:nil];
    
    if ([firstAnimalsNameArray containsObject:name] && ![name isEqual:@"banny"]) {
        SystemSoundID SoundID;
        NSString* pathToVoice = [[NSBundle mainBundle] pathForResource:name ofType:@"wav"];
        AudioServicesCreateSystemSoundID((__bridge CFURLRef) [NSURL fileURLWithPath:pathToVoice], &SoundID);
        AudioServicesPlaySystemSound(SoundID);
    }
    
    
    
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
