//
//  AnimalClass.h
//  HW_02
//
//  Created by Vitaliy Heras on 12/7/16.
//  Copyright © 2016 Vitaliy Heras. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface AnimalClass : NSObject


@property (strong,nonatomic)NSString* name;
@property (assign,nonatomic)NSNumber* age;
@property (strong,nonatomic)NSString* ownColor;
-(NSString*) voice;


@end
