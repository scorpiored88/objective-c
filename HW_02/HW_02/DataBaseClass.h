//
//  DataBaseClass.h
//  HW_02
//
//  Created by Vitaliy Heras on 12/6/16.
//  Copyright © 2016 Vitaliy Heras. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>
#import "addDataClass.h"

@interface DataBaseClass : NSObject <addDataDelegate>

@property (readonly, strong) NSPersistentContainer *persistentContainer;

@end
